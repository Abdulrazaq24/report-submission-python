# Stears Coding Homework: Using Python, Django & ReactJS

Interview submission repo for a simple post submission and retrieval built with Python, Djabgo and ReactJS.

## Getting Started
This submission contains two separate sections. Namely Task 1: Django application ( built with ReactJS and Python ) and Task 2: Ability to Search for post submissions ( Using ElasticSearch ).

### Setting up your dev environment
* Download and install [Anaconda](https://www.continuum.io/) -- for creating Python virtual enviroments.
* Create an Anaconda virtual environment to run Python 3.5 using the following command:

### Technology Stack
Stears uses a number of open source projects to work properly:

* [Python 3.5](https://www.python.org/downloads/release/python-350/)
* [React v15.4.2](https://facebook.github.io/react/)
* [JQuery 3.1.1](http://jquery.com/download/)
* [Twitter Bootstrap v3.3.7](http://getbootstrap.com)
* [ElasticSearch v2.4.4](http://elastic.co/downloads)

Other python packages used in this project are listed in the `requirements.txt` file in the project directory.

### Setup
* Download and install [Anaconda](https://www.continuum.io/)
* Change to the project (root) directory and create your development environment by executing the bootstrap script like thus:

```sh
$ ./bootstrap
```

* Once this is done, you can *activate* the Stears virtual environment using this command. You will always need to activate this environment anytime you want to run or work with Stears.

Once this is done, it would create a **Stears** virtual environment for development, and a *Fabric* virtual environment for deployment. You can **activate** the Stears virtual environment using this command.

Note that you will always need to activate this environment anytime you want to run or work with Stears.

```sh
$ source activate stears
```
OR
```sh
$ conda activate stears
```

### Installation
* Open terminal and navigate to the `stears` subdirectory. For example:

```sh
$ cd path/to/stears
```
* Ensure that you're in the right subdirectory by checking for the existence of a `README.md` file. You can do this by typing `ls` and inspecting the output.

* Run the following commands to finish the installation:

* Edit `config.py` in the project root path, if necessary.

* Launch terminal (or Command Prompt) for development purposes:

```sh
$ pip install --upgrade pip
$ pip install -r requirements.txt
$ pip install -e .
```

#### Database setup
After installation step completed, run:

```sh
$ alembic upgrade head
```
to initialize Stears database. 
Download [DB Browser for SQLite](https://sqlitebrowser.org/) to help view the table.

![Screenshot](./demo/screenshot2.png?raw=true "Screenshot")

#### Running Stears
* From Terminal, run:

```sh
$ stears
```
Note: this must be after you've successfully run *source activate stears* and *pip install e .* from your terminal.

![Screenshot](./demo/screenshot.png?raw=true "Screenshot")
![Screenshot](./demo/screenshot3.png?raw=true "Screenshot")
![Screenshot](./demo/screenshot7.png?raw=true "Screenshot")
![Screenshot](./demo/screenshot6.png?raw=true "Screenshot")
![Screenshot](./demo/screenshot9.png?raw=true "Screenshot")
![Screenshot](./demo/screensho11.png?raw=true "Screenshot")

##### Adding Sample Data
Navigate to `http://address-of-akhi/seed` or `http://127.0.0.1:5100/seed` from your local pc to seed sample data.

![Screenshot](./demo/screenshot22.png?raw=true "Screenshot")

Note: Don't forget to enable cookiens from http://127.0.0.1:5100/

##### Deployment
I've created a staging environment for Stears at root@`IP address` using Digital Ocean.
You'll need the private SSH key for the environment to proceed; you may create yours from Digital Ocean anytime.

* To deploy the application to the staging environment, follow these commands

```sh
$ cd /path/to/stears
$ source activate fabric
$ fab deploy -H root@`IP Address` -i /path/to/private/key
```

* To undeploy the application from the staging environment

```sh
$ cd /path/to/stears
$ source activate fabric
$ fab undeploy -H root@`IP Address` -i /path/to/private/key
```

* Finally you can use **fab --list** to see the list of fabric commands you can use for deployment.

## Elasticsearch (Task 2)
This project uses Elasticsearch to implement search. You will need to download [Elasticsearch v2.4.4](https://www.elastic.co/downloads/past-releases/elasticsearch-2-4-4) or latest release.

![Screenshot](./demo/screenshot8.png?raw=true "Screenshot")

## Wiki
[Stears Wiki](https://gitlab.com/abdulrazaq24/stears-test-submission-python/demo/)

## License
Copyright © 2020 Abdulrazaq.


## Tests
From the Terminal, run:

```sh
$ make test
```

## Credit
This project was developed by Abdulrazaq for the purpose of Stears Recruitment Process. For more information, contact <aimamisa@live.com>
