"""Initialize database

Revision ID: 609bc83d8520
Revises:
Create Date: 2017-01-30 18:42:37.376683

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
from sqlalchemy import func

revision = '609bc83d8520'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('user_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('email', sa.String, nullable=False, unique=True),
        sa.Column('password', sa.String, nullable=False),
        sa.Column('role', sa.String, nullable=False),
        sa.Column('password_reset_token', sa.String, nullable=True),
        sa.Column('created_on', sa.DateTime(), nullable=False),
    )

    op.create_table(
        'editor',
        sa.Column('editor_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.String, nullable=False),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('user.user_id'), primary_key=False)
    )

    op.create_table(
        'postdata',
        sa.Column('post_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('title', sa.String, nullable=False),
        sa.Column('body', sa.String, nullable=False),
        sa.Column('tag_name', sa.String, sa.ForeignKey('tag.name'), primary_key=False),
        sa.Column('posted_by', sa.String, sa.ForeignKey('editor.name'), primary_key=False),
        sa.Column('editor_id', sa.Integer, sa.ForeignKey('editor.editor_id'), primary_key=False),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('modified_at', sa.DateTime(), nullable=True)
    )

    op.create_table(
        'tag',
        sa.Column('tag_id', sa.Integer, primary_key=True, autoincrement=True),
        sa.Column('name', sa.String, nullable=False)
    )


def downgrade():
    op.drop_table('user')
    op.drop_table('admin')
    op.drop_table('editor')
    op.drop_table('postdata')
    op.drop_table('tag')
