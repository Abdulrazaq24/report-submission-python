import os

PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
EMAIL_TEMPLATES_PATH = os.path.join(PROJECT_PATH, 'email_templates')
DB_PATH = os.path.join(PROJECT_PATH, 'db.sqlite')
PORT = 5100

ENVIRONMENT = 'dev'
assert ENVIRONMENT in ('dev', 'staging', 'production')

API_URL = 'api/v1'
APP_URL = 'http://localhost:5100'

# MailGun
MAILGUN_API_KEY ="key-bbb35d80924cea9af66e58d239c7adfe"

# Twilio
TWILIO_API_KEY = "6d33d8e7b86072f7e9210392b9243259"
