"""Utility functions to facilitate sending emails."""
import requests

from stears.config import APP_URL, EMAIL_TEMPLATES_PATH, MAILGUN_API_KEY
import os


def load_email_template(filename, vars_dict):
    """Load email template."""
    if vars_dict:
        assert(isinstance(vars_dict, dict))

    path = os.path.join(EMAIL_TEMPLATES_PATH, filename)
    with open(path, 'r') as fp:
        email_body = '\n'.join(fp.readlines())

    if email_body:
        for placeholder, value in vars_dict.items():
            email_body = email_body.replace(placeholder, value)

    return email_body


def send_pwd_reset_email(display_name, user):
    """Send password reset email."""
    url_template = '{}/password/reset/{}'
    reset_url = url_template.format(APP_URL, user.password_reset_token)
    email_body = load_email_template('password-reset.txt',
                                     vars_dict={'{reset_url}': reset_url})
    template = 'Hello {}. Did you ask to reset your Stears password?'
    subject = template.format(display_name)
    response = send_email(email_body, subject)
    return response


def send_email(email_body, subject):
    """Send email using MailGun.

    Note:
        Sandbox sub-domains are for test purposes only. Please add your own
        domain or add the address to authorized recipients in Account Settings.
    """
    # TODO IMPORTANT replace with user.email,
    response = requests.post(
        'https://api.mailgun.net/v3/sandboxa9cb41f87bd748c6a936d276cd585950.mailgun.org/messages',
        auth=('api', MAILGUN_API_KEY),
        data={
            'from': 'Stears <postmaster@sandboxa9cb41f87bd748c6a936d276cd585950.mailgun.org>',
            'to': "Khalil <micaleel@gmail.com>",
            'subject': subject,
            'text': email_body
        }
    )
    return response
