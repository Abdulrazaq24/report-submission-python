from stears.models import User, Editor, Tag


def user_to_dict(User):
    return dict(
        user_id=User.user_id,
        email=User.email,
        role=User.role,
        created_on=User.created_on
    )


def editor_to_dict(Editor):
    return dict(
        editor_id=Editor.editor_id,
        name=Editor.name,
        user_id=Editor.user_id,
        user=user_to_dict(Editor.user)
    )


def tag_to_dict(Tag):
    return dict(
        tag_id=Tag.tag_id,
        name=Tag.name
    )
