"""Database helpers."""
import logging
import os
from hashlib import md5

from sqlalchemy import exists
import warnings
from stears.error import RecordExistsException
from stears.handlers import BaseHandler
from stears.models import Tag, User, Editor
from stears.utils.authutils import set_editor_auth_cookie, set_admin_auth_cookie

__all__ = ['tag_exists', 'user_exists']


def tag_exists(session, name):
    """Check whether a tag exists."""
    return session.query(exists().where(Tag.name == name)).scalar()


def user_exists(session, email):
    """Check whether a user exists in the database."""
    return session.query(exists().where(User.email == email)).scalar()


def add_tag(session, name):
    """Create a tag entry in the database."""
    if tag_exists(session=session, name=name):
        raise RecordExistsException()
    else:
        tag = Tag()
        tag.name = name
        session.add(tag)
        session.commit()


def add_admin(session, email, password):
    """Add a new admin record to the database."""
    user = add_user(session=session, email=email, password=password,
                    role='admin')

    session.add(user)
    session.commit()


def add_editor(session, email, password, name):
    """Add a new editor record to the database."""
    user = add_user(session=session, email=email, password=password,
                    role='editor')

    editor = Editor()
    editor.name = name
    editor.user_id = user.user_id
    editor.user = user

    session.add(editor)
    session.commit()


def add_user(session, email, password, role):
    """Add a new user record to the database.

    Returns:
        Instance of the User record.
    """
    assert role.lower() in ['editor', 'admin']
    if user_exists(session=session, email=email):
        raise RecordExistsException()
    else:
        password_hash = md5(password.encode('utf-8')).hexdigest()
        user = User(email=email, password=password_hash, role=role)
        session.add(user)
        session.flush()
        return user


def find_user_by_email(db, email):
    """Get a user based on a given email."""
    return db.query(User).filter(User.email == email).first()


def find_editor_by_email(db, email):
    """Get a editor based on a given email."""
    return db.query(Editor).filter(Editor.user.email == email).first()


def find_editor_by_user_id(db, user_id):
    """Load a editor record based on their user_id."""
    return db.query(editor).filter(editor.user_id == user_id).first()
