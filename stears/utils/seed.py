"""Script to seed the database with sample records."""
from stears.models import Tag
import random

from stears.utils import (user_exists, tag_exists, add_admin, add_editor, add_tag)

ADMIN_NAMES = ['Admin']
EDITOR_NAMES = ['Abdulrazaq', 'Preston', 'Bode', 'Abdulrahman']
TAG_NAMES = ['Microeconomic', 'Finance', 'Markets', 'Development', 'Government']


class Seeder(object):
    """Seeds the database with sample records."""

    def __init__(self, session):
        """Initialize seeder with SQLAlchemy session."""
        self.session = session

    def execute(self):
        """Run the seeder."""
        self.create_tags()
        self.create_editor()
        self.create_admin()

    def create_admin(self):
        """Create some of sample admin."""
        for person in ADMIN_NAMES:
            email = '{}@stearsng.com'.format(person.lower())
            if not user_exists(self.session, email=email):
                add_admin(self.session, email=email, password='password')

    def create_editor(self):
        """Create some of sample editor."""
        for person in EDITOR_NAMES:
            email = '{}@stearsng.com'.format(person.lower())
            if not user_exists(self.session, email=email):
                add_editor(self.session, email=email, password='password',
                           name=person.title())

    def create_tags(self):
        """Create some tags."""
        for t in TAG_NAMES:
            if not tag_exists(self.session, name=t.title()):
                add_tag(self.session, name=t.title())
