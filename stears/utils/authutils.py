from stears.handlers import BaseHandler
from stears.models import User, Editor
from tornado.escape import json_encode


def set_admin_auth_cookie(User, BaseHandler):
    cookie = {'email': User.email,
              'role': User.role,
              'user_id': User.user_id}
    BaseHandler.set_secure_cookie('user', json_encode(cookie))


def set_editor_auth_cookie(Editor, User, BaseHandler):
    """Set secure authentication cookie for logged in editor.

    Args:
        editor: Details of the logged in editor.
        user: The logged in user account corresponding to the editor.
        handler: Instance of the handler setting the cookie.
    """

    cookie = {'email': User.email,
              'display_name': Editor.name,
              'role': User.role,
              'user_id': Editor.user_id}
    BaseHandler.set_secure_cookie('user', json_encode(cookie))
