/**
 * Created by Abdulrazaq on 11/02/2017.
 */

// Prefix URL for page requests.
const baseUrl = window.location.protocol + '//' + window.location.host;

// Prefix URL for API requests.
const apiUrl = baseUrl + '/api/v1';


function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function defaultAJAXErrorHandler(jqXHR, textStatus, errorThrown) {
    console.error("ERROR:", jqXHR, textStatus, errorThrown);
}

