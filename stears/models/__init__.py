import datetime

import decimal

from sqlalchemy import Boolean
from sqlalchemy import Column, DateTime, String, Integer, ForeignKey, func
from sqlalchemy import Float
from sqlalchemy import Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm.attributes import QueryableAttribute

Base = declarative_base()

__all__ = ['User', 'Editor']


class Serializable(object):
    @staticmethod
    def date_to_str(d):
        if isinstance(d, datetime.datetime):
            return d.__str__()
        else:
            return d

    def as_dict(self):
        return {c.name: self.date_to_str(getattr(self, c.name)) for c in self.__table__.columns}


class User(Base):
    __tablename__ = 'user'
    __table_args__ = {'sqlite_autoincrement': True}
    user_id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    role = Column(String, nullable=False)
    password_reset_token = Column(String, nullable=True)
    created_on = Column(DateTime, nullable=False, default=func.now())
    
    def __repr__(self):
        return '<User({})'.format(self.email)


class Editor(Base, Serializable):
    __tablename__ = 'editor'
    editor_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(Integer, ForeignKey('user.user_id'))
    name = Column(String, nullable=False)

    user = relationship('User', foreign_keys=[user_id])

    def __repr__(self):
        return '<Editor({})'.format(self.name)


class PostData(Base, Serializable):
    __tablename__ = 'postdata'
    post_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String, nullable=False)
    body = Column(String, nullable=False)
    tag_name = Column(String, ForeignKey('tag.name'))
    posted_by = Column(String, ForeignKey('editor.name'))
    editor_id = Column(Integer, ForeignKey('editor.editor_id'))
    created_at = Column(DateTime, nullable=False, default=func.now())
    modified_at = Column(DateTime, nullable=True)

    editor = relationship('Editor', foreign_keys=[editor_id])
    editor = relationship('Editor', foreign_keys=[posted_by])
    tag = relationship('Tag', foreign_keys=[tag_name])

    def __repr__(self):
        return '<PostData({})'.format(self.title)


class Tag(Base, Serializable):
    __tablename__ = 'tag'
    tag_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)

    def __repr__(self):
        return '<Tag({})>'.format(self.name)


class Message(Base, Serializable):
    __tablename__ = 'message'
    message_id = Column(Integer, primary_key=True, autoincrement=True)
    body = Column(String, nullable=False)
    subject = Column(String, nullable=True, default='')
    date = Column(DateTime, nullable=False, default=func.now())
    from_user_id = Column(Integer, ForeignKey('user.user_id'))
    to_user_id = Column(Integer, ForeignKey('user.user_id'))
    status = Column(String, nullable=False, default='new')
    reply_message_id = Column(Integer, ForeignKey('message.message_id'), nullable=True)

    from_user = relationship('User', foreign_keys=[from_user_id])
    to_user = relationship('User', foreign_keys=[to_user_id])

    def __repr__(self):
        return '<Message({})'.format(self.body)
