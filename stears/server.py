import os

from tornado import autoreload
from tornado import httpserver
from tornado.ioloop import IOLoop
from tornado.options import define, options, parse_command_line

from stears.app import StearsApp

define("debug", default=True)
define("port", default=5100, type=int, help='Run on the given port')
define("run_bootstrap", default=True)
define("pid", default="tornado.pid", type=str, help="writes the PID of tornado to file")


def main():
    """Configures and starts the server."""
    try:
        if options.pid:
            with open(options.pid, 'w+') as f:
                f.write(str(os.getpid()))
        parse_command_line()
        app = StearsApp()

        http_server = httpserver.HTTPServer(app)
        http_server.listen(options.port)
        for _dir, _, files in os.walk('static'):
            [autoreload.watch(_dir + '/' + f) for f in files if not f.startswith('.')]
        print('Starting server at http://127.0.0.1:{}'.format(options.port))
        print('Quit the server with Control-C')
        IOLoop.instance().start()
        autoreload.start()
    except KeyboardInterrupt:
        if options.pid:
            print("\nDeleting PID file from the current working directory")
            os.remove(options.pid)


if __name__ == "__main__":
    main()
