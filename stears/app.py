import os

import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from tornado.options import options
from tornado.web import StaticFileHandler

from stears.config import DB_PATH
from stears.handlers import IndexHandler
from stears.handlers.admin import ROUTES_ADMIN
from stears.handlers.auth import ROUTES_AUTH
from stears.handlers.search import ROUTES_SEARCH
from stears.handlers.editor import ROUTES_EDITOR
from stears.handlers.dashboard import ROUTES_DASHBOARDS
from stears.handlers.profile import ROUTES_PROFILE
from stears.handlers.tags import ROUTES_TAGS
from stears.handlers.postdata import ROUTES_POSTDATA
from stears.handlers.submitpost import ROUTES_POSTSUBMIT
from stears.handlers.users import ROUTES_USERS
from stears.handlers.postrequest import ROUTES_POST_REQUEST
from stears.handlers.signup import ROUTES_SIGNUP


class StearsApp(tornado.web.Application):
    def __init__(self, debug=None):
        base_dir = os.path.dirname(__file__)

        # Setup SQLite with SQLAlchemy
        self.engine = create_engine('sqlite:///' + DB_PATH)
        self.session_maker = sessionmaker(bind=self.engine, autocommit=False, autoflush=True)
        self.session = scoped_session(self.session_maker)
        self.db = self.session()

        if not debug:
            if hasattr(options, 'debug'):
                debug = options.debug

        settings = dict(
            debug=debug,
            template_path=os.path.join(base_dir, "templates"),
            static_path=os.path.join(base_dir, "static"),
            cookie_secret='liTENOTOntan',
            login_url='/login',
            xsrf_cookies=False,
        )

        handlers = [
            # Landing page
            (r'/', IndexHandler),
            (r'/home', IndexHandler),

            # Static content
            (r'/(favicon.ico)', StaticFileHandler, {'path': ""}),
            (r'/static/(.*)', StaticFileHandler, {"path": "static/"}),
        ]

        handlers.extend(
            ROUTES_ADMIN + ROUTES_AUTH + ROUTES_EDITOR + ROUTES_DASHBOARDS + ROUTES_PROFILE + ROUTES_TAGS 
            + ROUTES_POSTSUBMIT + ROUTES_USERS + ROUTES_POSTDATA + ROUTES_POST_REQUEST + ROUTES_SIGNUP 
            + ROUTES_SEARCH
        )

        tornado.web.Application.__init__(self, handlers, **settings)
