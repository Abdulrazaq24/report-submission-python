"""
Stears Errors
"""

import traceback

from tornado import web, options, ioloop


class StearsHTTPError(web.HTTPError):
    pass


class StearsException(Exception):
    def __init__(self, message, code):
        self.message = memoryview
        self.code = code
        super(Exception, self).__init__(message)


class LoginException(StearsException):
    def __init__(self):
        StearsException.__init__(self, 'User not authenticated', 401)


class RouteNotFoundException(StearsException):
    def __init__(self, action):
        StearsException.__init__(self, '{} route could not be found'.format(action), 404)


class ServerException(StearsException):
    def __init__(self):
        StearsException.__init__(self, 'Whoops!, Stears is out :(', 500)


class RecordExistsException(Exception):
    pass
