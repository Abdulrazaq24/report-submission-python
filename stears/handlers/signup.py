from hashlib import md5

from sqlalchemy import exists

from stears.config import API_URL
from stears.error import RecordExistsException
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import User, Editor
from stears.utils import add_editor


class RegisterEditorApiHandler(ApiHandler):
    def data_received(self, chunk):
        pass

    def post(self):
        response = {'success': True, 'error': ''}

        try:
            session = self.get_session()
            add_editor(session,
                       email=self.get_argument("email"),
                       password=self.get_argument("password"),
                       name=self.get_argument("editorName"),
                       )

            # Login vendor and redirect to timeline.
            import json
            auth_cookie = {
                'email': self.get_argument('email'),
                'display_name': self.get_argument("editorName"),
                'role': 'editor'
            }
            self.set_secure_cookie('user', json.dumps(auth_cookie))

            self.respond(data=response)

        except RecordExistsException:
            return self.respond(data={'error': 'Editor exists'}, code=409)


class RegisterEditorPageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.render("signup-editor.html")


ROUTES_SIGNUP = [

    # PAGES
    (r'/signup', RegisterEditorPageHandler),

    # APIs
    (r'/{}/register/editor'.format(API_URL), RegisterEditorApiHandler),

]
