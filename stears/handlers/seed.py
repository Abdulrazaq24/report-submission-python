from stears.handlers import BaseHandler
from stears.utils.seed import Seeder


class SeedHandler(BaseHandler):
    """Seeds the database with sample records."""

    def get(self):
        """Handle page request to SeedHandler."""
        seeder = Seeder(session=self.get_session())
        seeder.execute()
        self.write('Database has been seeded with sample data')
