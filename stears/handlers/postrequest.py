import json

import requests
from tornado.web import authenticated, HTTPError

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import PostData, User, Editor

from twilio.rest import Client


class ConvertPostRequestApiHandler(ApiHandler):
    def get(self, post_id):
        session = self.get_session()
        request = session.query(PostData).filter(PostData.post_id == post_id).first()
        # TODO Handle situations where request cannot be found.
        request.status = 'in-progress'
        session.commit()
        print('Request {} converted'.format(request.post_id))
        # request.status = 'Converted'
        # session.commit()
        # print('Request {} converted'.format(request.post_id))
        return self.respond(data=dict(success=True))


class CompletePostRequestApiHandler(ApiHandler):
    def get(self, post_id):
        session = self.get_session()
        request = session.query(PostData).filter(PostData.post_id == post_id).first()
        # TODO Handle situations where request cannot be found.
        request.status = 'complete'
        session.commit()
        print('Request {} complete'.format(request.post_id))
        return self.respond(data=dict(success=True))


class PostRequestApiHandler(ApiHandler):
    """Handles API requests for PostRequests"""

    @authenticated
    def get(self):
        """Fetch all post requests made by a editor"""
        user = self.get_current_user_from_db()
        session = self.get_session()

        if user.role == 'editor':
            # Retrieve all tags created by current editor.
            editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()
            post_requests = session.query(PostData).filter(PostData.editor_id == editor.editor_id).all()

            return self.respond(data=dict(requests=[pr.as_dict() for pr in post_requests]))


class FetchPostRequestApiHandler(ApiHandler):
    """Handles API requests for Individual Post Requests"""

    def get(self, editor_id):
        """Fetch all posts belonging to a editor"""

        if not editor_id:
            self.respond(code=201, data={'msg': 'No Editor ID provided'})
        else:
            post_list = self.db.query(PostData). \
                filter(PostData.editor_id == editor_id).all()

            self.respond(data=dict(requests=[pl.as_dict() for pl in post_list]))


ROUTES_POST_REQUEST = [
    # APIs
    (r'/{}/requests'.format(API_URL), PostRequestApiHandler),
    (r'/{}/requests/view/(\w+)'.format(API_URL), FetchPostRequestApiHandler),
    (r'/{}/requests/(.+)/complete'.format(API_URL), CompletePostRequestApiHandler),
    (r'/{}/requests/(.+)/convert'.format(API_URL), ConvertPostRequestApiHandler),
]
