import json
import traceback

import datetime

import decimal
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from tornado.web import RequestHandler

from stears.config import DB_PATH
from stears.models import User
import json


Base = declarative_base()

__all__ = ['BaseHandler', 'JSONEncoder']


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        return json.JSONEncoder.default(self, o)


class BaseHandler(RequestHandler):
    def data_received(self, chunk):
        pass

    @property
    def db(self):
        return self.application.db

    def get_current_user_from_db(self):
        user_cookie = self.get_current_user()

        if user_cookie is None:
            return None

        user = self.db.query(User).filter(User.email == user_cookie['email']).first()
        return user

    @staticmethod
    def get_session():
        engine = create_engine('sqlite:///' + DB_PATH)
        session_maker = sessionmaker(bind=engine)
        Base.metadata.bind = engine
        session = session_maker()
        return session

    def get_current_user(self):
        user_cookie = self.get_secure_cookie("user")
        if user_cookie:
            return json.loads(user_cookie.decode('utf-8'))
        return None


class ApiHandler(BaseHandler):
    def respond(self, data, code=200):
        self.set_status(code)
        # self.write(JSONEncoder().encode({'status': code, 'data': data}))
        self.write(json.dumps({'status': code, 'data': data}, cls=DateTimeEncoder))
        self.finish()

    def write_error(self, status_code, **kwargs):
        self.set_header('Content-Type', 'application/json')
        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            # in debug mode, try to send a traceback
            lines = []
            for line in traceback.format_exception(*kwargs["exc_info"]):
                lines.append(line)
            self.finish(json.dumps({
                'error': {
                    'code': status_code,
                    'message': self._reason,
                    'traceback': lines,
                }
            }))
        else:
            self.finish(json.dumps({
                'error': {
                    'code': status_code,
                    'message': self._reason,
                }
            }))


class PageNotFoundHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def prepare(self):
        self.set_status(404)
        self.render('404.html')
