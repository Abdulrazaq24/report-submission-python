from sqlalchemy import text

from stears.handlers.core import ApiHandler
from stears.models import Editor


class EditorApiHandler(ApiHandler):
    def get(self, editor_id=None):
        session = self.get_session()

        # Attempt to retrieve a editor ID if it's included in the request.
        editor_id = self.get_argument("id") if 'id' in self.request.arguments else None

        if editor_id:
            editor = session.query(Editor).filter(Editor.editor_id == editor_id).first()
            payload = {'editor': editor.as_dict()}
        else:
            sql_text = text('select * from editor')  # IMPORTANT: Wrap SQL statements in text() function.
            editor = session.query(Editor).from_statement(sql_text).all()

            payload = {
                # HACK: Convert SQLAlchemy objects to dictionaries for easy serialization.
                'editor': [e.as_dict() for e in editor]
            }

        self.write(payload)


ROUTES_EDITOR = [
    (r'/api/v1/editor/([0-9])*', EditorApiHandler),

]
