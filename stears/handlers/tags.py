from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import Tag


class TagsPageHandler(BaseHandler):
    def get(self):
        tags = self.get_session().query(Tag).all()
        has_tags = True if tags and len(tags) > 0 else False
        self.render('tags.html', tags=tags, has_tags=has_tags, user=self.current_user)

    def post(self):
        name = self.get_argument('name')
        session = self.get_session()

        s = Tag()
        s.name = name

        session.add(s)
        session.flush()
        session.commit()

        self.redirect('/tags?saved=1')

class TagsApiHandler(ApiHandler):
    def get(self):
        tags = self.get_session().query(Tag).all()
        return self.respond(data=dict(tags=[t.as_dict() for t in tags]))

    def delete(self, tag_id):
        session = self.get_session()
        tag = session.query(Tag).filter(Tag.tag_id == tag_id).first()

        if tag is not None:
            session.delete(tag)
            session.commit()
        else:
            return self.respond(
                data=dict(success=False, error='Cannot find tag with that ID'),
                code=404
            )

class EditTagPageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, tag_id):
        """Returns tag details."""
        session = self.get_session()
        tag = session.query(Tag).filter(Tag.tag_id == tag_id).first()

        if not tag:
            # Tag record cannot be found; display 404.
            self.render('404.html')
        else:
            params = dict(tag_id=tag_id, tag=tag)
            self.render('edit-tag.html', **params)

class EditTagHandler(ApiHandler):
    """Handles API requests for editing Tag name by Admin"""

    def post(self, tag_id):
        session = self.get_session()
        tag = session.query(Tag).filter(Tag.tag_id == tag_id).first()

        tag.name = self.get_argument("tagName")
        self.application.db.commit()

        return self.respond(data=dict(success='Tag details updated successfully'))


ROUTES_TAGS = [
    (r'/tags', TagsPageHandler),
    (r'/{}/tags'.format(API_URL), TagsApiHandler),
    (r'/{}/tags/(\w+)'.format(API_URL), TagsApiHandler),
    (r'/tags/(.*)/edit', EditTagHandler),
    (r'/tag/edit/(\w+)', EditTagPageHandler),
]
