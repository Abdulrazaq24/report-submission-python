"""Handlers for administrative pages."""

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.handlers.seed import SeedHandler
from stears.models import User, Editor, PostData
from stears.utils.authutils import (set_editor_auth_cookie)
from tornado.web import authenticated
from stears.utils.adapters import user_to_dict

class AccountsPageHandler(BaseHandler):
    """Handles request for accounts page."""

    def get(self):
        editor = self.get_session().query(Editor).all()
        has_editor = True if editor and len(editor) > 0 else False
        self.render('accounts.html', editor=editor, has_editor=has_editor, user=self.current_user)


class PostsPageHandler(BaseHandler):
    """Handles request for posts page."""

    @authenticated
    def get(self):
        """Handle page request to posts page."""
        result = self.render('posts-page.html', user = self.get_current_user())


class AdminPageHandler(BaseHandler):
    """Handles requests to the admin page."""

    @authenticated
    def get(self):
        """Handle page requests to the admin page."""
        user = self.get_current_user_from_db()
        session = self.get_session()

        if user.role == 'admin':
            # Checks if current user account is admin
            self.render('admin.html', user = self.get_current_user())

        self.redirect('/home')


ROUTES_ADMIN = [
    (r'/seed', SeedHandler),
    (r'/accounts', AccountsPageHandler),
    (r'/posts-page', PostsPageHandler),
    (r'/admin', AdminPageHandler),
]
