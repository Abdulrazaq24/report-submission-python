from tornado.web import authenticated

from stears.error import ServerException
from stears.handlers import BaseHandler


class DashboardNewHandler(BaseHandler):
    def get(self):
        self.render('dashboard-new.html')


class DashboardHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    @authenticated
    def get(self):
        user = self.get_current_user()
        role = user['role']
        params = dict(user=user)

        if role == 'editor':
            self.render("dashboard-editor.html", **params)
        else:
            raise ServerException()


ROUTES_DASHBOARDS = [
    (r'/dashboard', DashboardHandler),
    (r'/dashboard/new', DashboardNewHandler),
]
