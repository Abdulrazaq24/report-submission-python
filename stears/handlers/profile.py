from tornado.web import authenticated

from stears.error import ServerException
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import Editor


class ProfileEditorApiHandler(ApiHandler):
    """Handles API requests about editor profiles"""

    def data_received(self, chunk):
        pass

    def get(self, *args, **kwargs):
        self.respond(data={'editor': 'Sample'})


class EditorProfilePageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    @authenticated
    def get(self):
        user = self.get_current_user()
        role = user['role']
        params = dict(user=user)

        if role == 'editor':
            self.render("profile.html", **params)
        else:
            raise ServerException()


ROUTES_PROFILE = [
    # Customer Profile
    (r'/profile', EditorProfilePageHandler),
    (r'/api/v1/profile/editor/(\w+)', ProfileEditorApiHandler),
]
