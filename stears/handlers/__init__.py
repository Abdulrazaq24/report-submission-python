"""Stears Handlers."""
from stears.handlers.core import BaseHandler


class IndexHandler(BaseHandler):
    """Handles page requests made to the landing page."""

    def get(self, tag_id = None):
        """Handle requests made to the landing page.
        Redirects logged in user to the appropriate page.
        Raises:
            An exception if the logged in user has an unrecognized role.
        """
        user = self.get_current_user()
        params = dict(tag_id = tag_id, user = user)

        if user is None:
            self.render('posts.html', **params)
        elif user['role'] == 'admin':
            self.render('admin.html', **params)
        elif user['role'] == 'editor':
            self.render('posts.html', **params)
        else:
            raise Exception('Unknown user role')
