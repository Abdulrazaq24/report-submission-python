import json

import requests
from tornado.web import authenticated, HTTPError

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import Tag, PostData, Editor, User

from twilio.rest import Client

class SubmitPostPageHandler(ApiHandler):
    """Handles API requests for SubmitPosts"""
    def get(self):
        self.render('data-page.html')


class SubmitPostApiHandler(ApiHandler):
    """Handles API requests for SubmitPosts"""

    @authenticated
    def get(self):
        """Fetch all tags and industries added by admin"""
        user = self.get_current_user_from_db()
        session = self.get_session()

        # Retrieve all tags & industries from the database
        post_tags = session.query(PostData).filter(PostData.tag_name == tag.name).all()

        return self.respond(
            data=dict(requests=[
                (pt.as_dict() for pt in post_tags)
                ]
            )
        )


    def post(self):
        """Saves a new post detail by editor to the database"""
        title = self.get_argument('title')
        body = self.get_argument('body')
        tag_name = self.get_argument('tag_name')

        session = self.get_session()

        if self.get_current_user()['role'] == 'admin':
            return self.respond(data=dict(error="Admins cannot submit post data"), code=401)

        user = session.query(User).filter(User.email == self.get_current_user()['email']).first()
        editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()

        post_submission = PostData()
        post_submission.title = title
        post_submission.body = body
        post_submission.tag_name = tag_name

        post_submission.posted_by = editor.name
        post_submission.editor_id = editor.editor_id

        session.add(post_submission)
        session.flush()
        session.commit()

        self.respond(
            data={
                'success': True
            }
        )

ROUTES_POSTSUBMIT = [
    # APIs
    (r'/{}/data'.format(API_URL), SubmitPostApiHandler),

    # PAGE
    (r'/submit-data', SubmitPostPageHandler),
]
