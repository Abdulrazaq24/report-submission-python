import uuid
from hashlib import md5

import requests

from stears.utils import find_user_by_email, find_editor_by_user_id
from stears.utils.emailhelpers import send_pwd_reset_email
from tornado.escape import json_encode

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import User, Editor
from tornado.web import authenticated

from twilio.rest import Client

class LogoutApiHandler(ApiHandler):
    """API handler for processing logout requests."""

    @authenticated
    def get(self):
        """Log current user out."""
        self.clear_cookie('user')
        self.respond(data={'success': True})


class LoginApiHandler(ApiHandler):
    """API handler for handling login requests."""

    def post(self):
        """Attempt logging in user."""
        username = self.get_argument("username")
        password = self.get_argument("password")

        db = self.application.db
        password_hash = md5(password.encode('utf-8')).hexdigest()
        user = db.query(User).filter(User.email == username).first()

        if user and user.password == password_hash:
            if user.role == 'editor':
                entity = db.query(Editor).filter(Editor.user_id == user.user_id).first()
                cookie = {'email': username,
                          'display_name': entity.name,
                          'role': user.role,
                          'user_id': entity.user_id}
            elif user.role == 'admin':
                cookie = {'email': username,
                          'role': user.role,
                          'user_id': user.user_id}
            else:
                raise ValueError('Unrecognized user role {}'.format(user.role))

            self.set_secure_cookie('user', json_encode(cookie))
            self.set_secure_cookie('failedAttempts', str(0))  # reset number of failed login attempts
            self.set_status(200)
            self.write({'success': 'true'})
        else:
            failed_attempts = self.get_secure_cookie("failedAttempts")
            if not failed_attempts:
                failed_attempts = 0
            self.set_status(401)
            self.set_secure_cookie("failedAttempts", str(int(failed_attempts) + 1))
            self.write({'success': 'false', 'failedAttempts': int(failed_attempts)})


class LoginHandler(BaseHandler):
    """Handler for page requests made to login page"""

    def get(self):
        """Serve the login page."""
        self.render('login.html')


class LogoutHandler(BaseHandler):
    """Handler for page requests made to log out current user."""

    @authenticated
    def get(self):
        """Log out current user."""
        self.clear_cookie('user')
        self.redirect(self.get_argument('next', '/'))


class PasswordRecoveryHandler(BaseHandler):
    """Handles page requests to password recovery page."""

    def get(self):
        """Serve the password recovery page."""
        self.render('password-recovery.html')


class PasswordRecoveryApiHandler(ApiHandler):
    """Handles API requests made to recover account password"""

    def post(self):
        user = find_user_by_email(self.db, email=self.get_argument("email"))

        if not user:
            self.respond({'error': 'Cannot find an account with that email'})
        else:
            if user.role == 'editor':
                entity = find_editor_by_user_id(self.db, user_id=user.user_id)

            password_reset_token = str(uuid.uuid4())
            user.password_reset_token = password_reset_token
            self.db.commit()

            response = send_pwd_reset_email(display_name=entity.name, user=user)
            self.respond(data={'success': 'Password reset email sent.',
                               'response_status_code': response.status_code})


class PasswordResetHandler(BaseHandler):
    """Handles password request links clicked from emails"""

    def get(self, token):
        user = self.db.query(User).filter(User.password_reset_token == token).first()

        if user is None:
            self.render('404.html')
        else:
            self.render('password-reset.html', token=token)


class PasswordResetApiHandler(ApiHandler):
    """Handles API requests made to reset account password"""

    def post(self):
        password = self.get_argument("password")
        password_reset_token = self.get_argument("token")

        session = self.get_session()
        user = session.query(User).filter(User.password_reset_token == password_reset_token).first()

        if not user:
            return self.respond(data={'error': 'Invalid password reset token'}, code=404)
        else:
            print('type(password)', type(password))
            user.password = md5(password.encode('utf-8')).hexdigest()
            user.password_reset_token = None
            session.commit()

            return self.respond(data={'success': 'Password reset successfully'})


class PasswordUpdateApiHandler(ApiHandler):
    """Handles requests by logged in users to change their passwords"""

    @authenticated
    def post(self):
        old_password = self.get_argument("oldPassword")
        new_password = self.get_argument("newPassword")

        user = self.get_current_user_from_db()

        old_password_hash = md5(old_password.encode('utf-8')).hexdigest()

        if user and user.password != old_password_hash:
            return self.respond(data=dict(error='Wrong password'), code=401)
        else:
            user.password = md5(new_password.encode('utf-8')).hexdigest()
            user.password_reset_token = None
            self.application.session.commit()

            if user.role == 'editor':
                entity = self.db.query(Editor).filter(Editor.user_id == user.user_id).first()

            display_name = entity.name

            # Compose email
            body = 'Hi,\n\nYour Stears password has been changed successfully.\n\n'
            body += 'If you did not make this change yourself, please contact IT department to resolve this.\n\n'
            body += 'Otherwise, We hope you feel a lot more secure now\n\n'
            body += 'Your new Password is: '
            body += new_password
            body += '\n\n\n'
            body += 'Thanks,\nStears App'

            # Sandbox subdomains are for test purposes only.
            # Please add your own domain or add the address to authorized recipients in Account Settings.
            mg_response = requests.post(
                "https://api.mailgun.net/v3/client.stearsng.com/messages",
                auth=("api", "b02b377de8aa0b142de7461e7f32276e-060550c6-4e3dd996"),
                data={
                    "from": "Stears <postmaster@client.stearsng.com>",
                    "to": user.email,  # TODO IMPORTANT replace with user.email,
                    "subject": "Hello {}. Password Changed".format(display_name),
                    "text": body
                }
            )

            print(mg_response, mg_response.content)

            self.respond(
                data={
                    'success': 'Password changed successfully. Check your inbox!',
                    'response_status_code': mg_response.status_code
                }
            )


ROUTES_AUTH = [
    # Pages
    (r'/login', LoginHandler),
    (r'/signin', LoginHandler),
    (r'/logout', LogoutHandler),
    (r'/signout', LogoutHandler),
    (r'/password/recover', PasswordRecoveryHandler),
    (r'/password/reset/(.+)', PasswordResetHandler),

    # APIs
    (r'/{}/login'.format(API_URL), LoginApiHandler),
    (r'/{}/logout'.format(API_URL), LogoutApiHandler),
    (r'/{}/password/recover'.format(API_URL), PasswordRecoveryApiHandler),
    (r'/{}/password/reset'.format(API_URL), PasswordResetApiHandler),
    (r'/{}/password/update'.format(API_URL), PasswordUpdateApiHandler),

]
