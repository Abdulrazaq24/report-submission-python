"""Search Handlers."""
import pprint

from elasticsearch import Elasticsearch
from sqlalchemy import text

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import Editor
from stears.utils.adapters import editor_to_dict
from tornado.escape import json_encode


class SearchPageHandler(BaseHandler):
    """Renders search page."""

    def get(self):
        """Render search page"""
        query = self.get_argument('q', '')
        self.render('search.html', query=query)


class SearchApiHandler(ApiHandler):
    """Handles search queries."""

    def get(self):
        """Handle a search query."""
        query = self.get_argument('q', '')
        es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

        query_es = {
            "query": {
                "bool": {
                    "must": [],
                    "must_not": [],
                    "should": [{
                        "fuzzy": {
                            "body_text": {
                                "value": "*{}*".format(query)}}}]
                }
            },
            "from": 0,
            "size": 10,
            "sort": [],
            "aggs": {}
        }

        pprint.pprint(query_es)
        results = es.search(
            index='stears',
            body=query_es)

        post_ids = set()
        hit_count = results['hits']['total']
        print('hit_count: ', hit_count)
        if hit_count > 0:
            for hit in results['hits']['hits']:
                post_id = hit['_source']['post_id']
                post_ids.add(post_id)

            post_ids_str = [str(p) for p in post_ids]
            print('Post IDs: ' + ', '.join(post_ids_str))
            sql_text = text('select * from post where post_id in ({})'.format(','.join(post_ids_str)))
            posts = self.db.query(PostData).from_statement(sql_text).all()
            list_of_posts_dict = [post_to_dict(p) for p in posts]
            pprint.pprint(list_of_posts_dict)
            self.respond(data={'posts': list_of_posts_dict})
        else:
            self.write("No results")

class ReIndexHandler(BaseHandler):
    """Re-indexes post records."""

    def get(self):
        """Handle a search query."""
        es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
        posts = self.db.query(PostData).all()

        for post in posts:
            p_dict = post.as_dict()
            p_dict['tag_name'] = post.tag.name
            tokens = [str(p).strip().lower() for k, p in p_dict.items()
                      if p is not None and len(str(p).strip()) > 0 and k not in ('post_id', 'tag_id', 'user_id')]
            body = dict(post_id=post.post_id, body_text=' '.join(tokens))

            es.index(index='stears', doc_type='post', id=post.post_id, body=json_encode(body))
        self.write('Indexing complete.')


ROUTES_SEARCH = [
    (r'/search', SearchPageHandler),
    (r'/api/v1/search'.format(API_URL), SearchApiHandler),
    (r'/search/reindex', ReIndexHandler)
]
