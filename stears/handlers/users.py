from stears.config import API_URL
from stears.handlers.core import ApiHandler
from stears.handlers import BaseHandler
from stears.models import User, Editor
from stears.utils.adapters import user_to_dict

class UsersHandler(BaseHandler):
    """Handler for page requests to user admin page."""
    def get(self):
        session = self.get_session()
        users = session.query(User).all()
        self.render('users.html', users=users)

class UsersApiHandler(ApiHandler):
    """Handler for retrieving all Users to be displayed on the Admin page"""
    def get(self):
        session = self.get_session()
        users = session.query(User).all()

        return self.respond(data=dict(users=[user_to_dict(u) for u in users]))

    def delete(self,user_id):
        session = self.get_session()
        user = session.query(User).filter(User.user_id == user_id).first()

        if user is not None:
            #Check if user is Vendor/Customer then delete from appropriate table in database
            if user.role == 'editor':
                editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()
                session.delete(editor)
                session.commit()

            session.delete(user)
            session.commit()
            return self.respond(data=dict(success=True))
        else:
            return self.respond(
                data=dict(success=False, error='Cannot find user with that ID'),
                code=404
            )


class ViewUserPageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, user_id):
        session = self.get_session()

        #Check if User_id was provided in URL then execute appropriately
        if user_id is None:
            return self.respond(
                data=dict(success=False, error='Cannot find user with that ID'),
                code=404
            )
        else:
            user = session.query(User).filter(User.user_id == user_id).first()

            #Check if user is Vendor/Customer then pull data from appropriate table in database
            if user.role == 'editor':
                editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()

                params = dict(user=user, editor=editor, success=True)
                return self.render('view-editor.html', **params)


class EditUserHandler(ApiHandler):
    """Handles API requests for editing User details by Admin"""

    def get(self, user_id):
        """Returns user details."""
        session = self.get_session()
        user = session.query(User).filter(User.user_id == user_id).first()

        if user.role == 'editor':
            editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()
            self.render('edit-editor.html', user=user, editor=editor)
        else:
            return self.respond(data=dict(success=False, error='Only editor can edit their details'), code=401)

    def post(self, user_id):
        session = self.get_session()
        user = session.query(User).filter(User.user_id == user_id).first()

        if user.role == 'editor':
            editor_name = self.get_argument("editorName")
            editor_email = self.get_argument("editorEmail")

            editor = self.db.query(Editor).filter(Editor.user_id == user_id).first()

            editor.name = editor_name
            user.email = editor_email
            self.application.db.commit()

        return self.respond(data=dict(success='User details updated successfully'))


ROUTES_USERS = [
    (r'/users', UsersHandler),
    (r'/{}/users'.format(API_URL), UsersApiHandler),
    (r'/users/(.*)/edit', EditUserHandler),
    (r'/user/edit/(\w+)', EditUserHandler),
    (r'/{}/users/(\w+)'.format(API_URL), UsersApiHandler),
    (r'/view-user/(\w+)', ViewUserPageHandler),
]
