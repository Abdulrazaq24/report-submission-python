import json

import requests
from tornado.web import authenticated, HTTPError

from stears.config import API_URL
from stears.handlers import BaseHandler
from stears.handlers.core import ApiHandler
from stears.models import PostData, User, Editor


class PostDataPageHandler(BaseHandler):
    def get(self):
        posts = self.get_session().query(PostData).all()
        has_posts = True if posts and len(posts) > 0 else False
        self.render('post-page.html', posts=posts, has_posts=has_posts, user=self.current_user)

class PostDataApiHandler(ApiHandler):
    def get(self):
        posts = self.get_session().query(PostData).all()
        return self.respond(data=dict(posts=[p.as_dict() for p in posts]))

    def delete(self, post_id):
        session = self.get_session()
        postdata = session.query(PostData).filter(PostData.post_id == post_id).first()

        if postdata is not None:
            session.delete(postdata)
            session.commit()
        else:
            return self.respond(
                data=dict(success=False, error='Cannot find post data with that ID'),
                code=404
            )

class EditorPostDataApiHandler(ApiHandler):
    @authenticated
    def get(self):
        """Fetch all tags requests made by a editor"""
        user = self.get_current_user_from_db()
        session = self.get_session()

        if user.role == 'editor':
            # Retrieve all posts that match current editor's post_id
            editor = session.query(Editor).filter(Editor.user_id == user.user_id).first()
            tag_requests = session.query(Request).filter(Request.post_id == editor.post_id).all()

        return self.respond(data=dict(requests=[tr.as_dict() for tr in tag_requests]))

class EditPostDataPageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, post_id):
        """Returns post details."""
        session = self.get_session()
        postdata = session.query(PostData).filter(PostData.post_id == post_id).first()

        if not postdata:
            # Cliend data record cannot be found; display 404.
            self.render('404.html')
        else:
            params = dict(post_id=post_id, postdata=postdata)
            self.render('edit-post.html', **params)

class EditPostDataHandler(ApiHandler):
    """Handles API requests for editing Post details by Admin"""

    def post(self, post_id):
        session = self.get_session()
        postdata = session.query(PostData).filter(PostData.post_id == post_id).first()

        postdata.title = self.get_argument("title")
        postdata.body = self.get_argument("body")
        postdata.modified_at = modified_at
        
        self.application.db.commit()

        return self.respond(data=dict(success='Post details updated successfully'))


class ViewPostPageHandler(BaseHandler):
    def data_received(self, chunk):
        pass

    def get(self, post_id):
        session = self.get_session()

        #Check if Post_id was provided in URL then execute appropriately
        if post_id is None:
            return self.respond(
                data=dict(success=False, error='Cannot find post with that ID'),
                code=404
            )
        else:
            post = session.query(PostData).filter(PostData.post_id == post_id).first()

            params = dict(post=post, success=True)
            return self.render('view-post.html', **params)


ROUTES_POSTDATA = [
    (r'/posts-page', PostDataPageHandler),
    (r'/{}/posts'.format(API_URL), PostDataApiHandler),
    (r'/{}/posts/editor'.format(API_URL), EditorPostDataApiHandler),
    (r'/{}/posts/(\w+)'.format(API_URL), PostDataApiHandler),
    (r'/posts/(.*)/edit', EditPostDataHandler),
    (r'/postdata/edit/(\w+)', EditPostDataPageHandler),
    (r'/view-post/(\w+)', ViewPostPageHandler)
]
