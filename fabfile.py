#!/usr/bin/env python
"""Fabfile for deploying Stears."""

import os
import sys
import shutil
import traceback
from contextlib import contextmanager as context

from fabric.api import local, run, cd, sudo, prefix, execute, task, env, hosts
from fabric.operations import put
from fabric.contrib.project import rsync_project
from fabric.context_managers import shell_env as export

# setup connection
env.use_ssh_config = True
env.hosts = [""] # DigitalOcean IP address
env.user = "root" # Droplet username
env.key_filename = "/Users/abdulrazaq/.ssh/id_rsa" # Path to SSH key
env.password = ""
env.port = 22 # Default port

version = "0.9"
root = "~/abdulrazaq/python"
home = os.path.join(root, "stears")

dist = "dist"
project = 'stears-%s' % version
base = os.path.join(home, project)
application = os.path.join(base, "stears")
env = os.path.join(base, '.env')

@context
def use(env):
    """Activate a particular the virtual environment in the cloud."""
    with cd(base):
        expression = "source %s/bin/activate" % env
        with prefix(expression):
            yield


def mkvirtualenv(env):
    """Create a new virtual environment in the cloud."""
    if run('test -d {0}'.format(env), warn_only=True).failed:
        run("virtualenv %s" % env)


def virtualenv(env):
    """Create a virtual environment locally."""
    local("virtualenv %s" % env)


def package():
    """Package & Transfer most version of the application to the server."""
    header("packaging and transferring application to the server")
    print("updating nginx configuration on the server")
    sudo("test -d {0} || mkdir -p {0}".format("~/nginx/conf/backup"))
    succeeded = run("test -f {0}".format("/etc/nginx/sites-enabled/default"),
                    warn_only=True).succeeded
    if succeeded:
        print("Moving the default nginx configuration to the backup directory")
        sudo("mv {0} ~/nginx/conf/backup/".format(
                "/etc/nginx/sites-enabled/default"))
    put("./settings/stears.conf", "/etc/nginx/conf.d/", use_sudo=True)
    print("Transferring the current version of the application to the server")
    archive = "stears-{0}.tar.gz".format(version)
    local("git archive -o {0} HEAD".format(archive))
    put("./{0}".format(archive), base, use_sudo=True)
    with cd(base):
        run("tar -xvf {0}".format(archive))
        run("rm {0}".format(archive))
        with use(env):
            print("Installing project dependencies...")
            sudo("pip install --upgrade pip")
            sudo("pip install -r requirements.txt")
    local("rm {0}".format(archive))
    print("Finished transferring the application")


@task
def setup():
    """Install dependencies for the project on the server."""
    sudo("apt-get update")
    sudo("apt-get -y install nginx")
    sudo("apt-get -y install git")
    sudo("apt-get -y install python3-pip")
    sudo("pip3 install --upgrade pip")
    sudo("pip3 install virtualenv")
    try:
        with cd(base):
            mkvirtualenv(env)
    except OSError as e:
        print('Virtual environment already exists', e)
        traceback.print_exc(file=sys.stdout)


@task
def start():
    """Serve application using nginx+tornado in the cloud."""
    sudo("service nginx start")
    with cd(application):
        with use(env):
            variables = {}
            with export(**variables):
                run("python stears/server.py --port=8080 --pid=tornado.pid",
                    pty=False, warn_only=True)


@task
def migrate():
    """Run alembic migrations in the cloud."""
    with use(env):
        run("pip install -e .")
        with cd(application):
            run("alembic upgrade head")


@task
def stop():
    """Stop serving application in the cloud; kill tornado and stop nginx."""
    with cd(base):
        result = run("test -f tornado.pid", warn_only=True)
        if not result.failed:
            print("Stopping the tornado process.")
            run("kill `cat tornado.pid`")
            run("rm tornado.pid")
    sudo("service nginx stop")


@task
def undeploy():
    """Clean out the old install from the server."""
    print("Cleaning out old install of stears")
    execute(stop)
    sudo("rm -rf %s" % base)
    sudo("rm /etc/nginx/conf.d/stears.conf")
    print("Finished cleaning out the old installation")


@task
def deploy():
    """Setup and run the website."""
    print("Creating required application directories")
    for name in (root, home, base,):
        run("test -d {0} || mkdir -p {0}".format(name))
    execute(setup)
    execute(package)
    print("Applying migrations to the database in the cloud...")
    execute(migrate)
    execute(start)


@task
def redeploy():
    """Deploy to a running version of Stears.

    This works by rsync the project directory with the running deploy and
    restart.
    """
    with cd(base):
        result = run("test -f tornado.pid", warn_only=True)
        if not result.failed:
            print("Tornado is currently running, stopping it")
            execute(stop)
    print("Updating nginx configuration on the server")
    put("./settings/stears.conf", "/etc/nginx/conf.d/", use_sudo=True)
    print("Synchronizing the project directory")
    excluded = [
        "*.db", ".env", "*.pyc", "*.pyo", ".git"
    ]
    rsync_project(remote_dir=base, local_dir="./", exclude=excluded)
    print("Finished transferring the application,")
    print("Applying migrations to the database in the cloud")
    execute(migrate)
    print("Restarting tornado and nginx...")
    execute(stop)
    execute(start)


def header(message, char="*"):
    """Format messages within this script in a particular way."""
    width = 70
    padding = (width - len(message))/2
    print(char * padding + message + char * padding)


@task
def clean():
    """Remove all build related stuff."""
    header("clean")
    dir = os.getcwd()
    print("Working directory: %s" % dir)
    print("Removing all .pyc and temporary files")
    for root, dirs, files in os.walk(dir):
        for f in files:
            deletable = [".pyc", ".tar.gz", "~", ".sqlite", ".db"]
            for extension in deletable:
                if f.endswith(extension):
                    path = os.path.join(root, f)
                    print("Removing: %s" % path)
                    os.unlink(path)
        for d in dirs:
            if d.endswith('.egg-info'):
                path = os.path.join(root, d)
                print("Removing Directory: %s" % path)
                shutil.rmtree(path)
    header('')
