"""Unit tests for IndexHandler."""
import unittest
from unittest import mock

from stears.app import StearsApp
from stears.handlers import IndexHandler
from tornado.testing import AsyncHTTPTestCase


class RoutesTestCase(AsyncHTTPTestCase):
    """Unit tests for routes."""

    def get_app(self):
        """Create an instance of the application."""
        return StearsApp(debug=False)

    def test_homepage_anonymous(self):
        """Test that anonymous users are directed to appropriate page."""
        response = self.fetch('/')
        self.assertEqual(response.code, 200)

    @staticmethod
    def _get_editor_cookie():
        """Get a cookie that'll be stored if a editor is logged in."""
        cookie = {'email': 'tony@gmail.com',
                  'display_name': 'Tony',
                  'role': 'editor',
                  'user_id': 1}
        return cookie

    def test_homepage_editor(self):
        """Test that appropriate page is displayed for logged in editor."""
        cookie = self._get_editor_cookie()
        with mock.patch.object(IndexHandler, 'get_current_user') as m:
            m.return_value = cookie

            response = self.fetch('/', method='GET')
        markup = str(response.body.lower())
        self.assertTrue('View posts'.lower() in markup)
        self.assertTrue(cookie['display_name'].lower() in markup)

if __name__ == '__main__':
    unittest.main()
