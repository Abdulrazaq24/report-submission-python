"""
Stears setup
For development:
    `python setup.py develop` or `pip install -e .`
"""

from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

if __name__ == '__main__':
    requirements_path = os.path.join(here, "requirements.txt")
    requirements = open(requirements_path, 'r').readlines()
    print(type(requirements))
    setup(
        name='stears',
        description='Stears Test submission',
        packages=find_packages(),
        version='0.1.0',
        author='Abdulrazaq',
        author_email='aimamisa@live.com',
        license='All Rights Reserved',
        install_requires=requirements,
        entry_points={
            'console_scripts': [
                'stears = stears.server:main',
            ]
        },
    )
