clean:
	@find . -name \*.pyc -delete

test:
	@python -m unittest discover

dbreset:
	@rm test -f stears/db.sqlite && sqlite stears/db.sqlite
	@cd stears/ && alembic upgrade head && cd -

dbupgrade:
	@cd stears/ && alembic upgrade head && cd -

run: test
	python stears/server.py --port=6000 --debug
